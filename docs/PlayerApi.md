# PlayerApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**forget**](PlayerApi.md#forget) | **DELETE** /api/player/forget | 🙈 Forget your current progress.
[**get**](PlayerApi.md#get) | **GET** /api/player | 👤 Obtain information about yourself.
[**register**](PlayerApi.md#register) | **POST** /api/player/register | 📝 Register yourself here.


<a name="forget"></a>
# **forget**
> forget()

🙈 Forget your current progress.

👻 This allows you to re-register with a different name, and even repeat the mazes that you have played before.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PlayerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

PlayerApi apiInstance = new PlayerApi();
try {
    apiInstance.forget();
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerApi#forget");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="get"></a>
# **get**
> PlayerInfo get()

👤 Obtain information about yourself.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PlayerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

PlayerApi apiInstance = new PlayerApi();
try {
    PlayerInfo result = apiInstance.get();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerApi#get");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PlayerInfo**](PlayerInfo.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="register"></a>
# **register**
> register(name)

📝 Register yourself here.

You need to register to be able to start navigating through mazes.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.PlayerApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

PlayerApi apiInstance = new PlayerApi();
String name = "name_example"; // String | The name you wish to represent you in the leader board.
try {
    apiInstance.register(name);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerApi#register");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| The name you wish to represent you in the leader board. |

### Return type

null (empty response body)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

