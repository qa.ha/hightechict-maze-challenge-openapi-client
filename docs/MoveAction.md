
# MoveAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**direction** | [**DirectionEnum**](#DirectionEnum) | What is the relative direction of this tile. |  [optional]
**isStart** | **Boolean** | Is this the tile where the maze begun. |  [optional]
**allowsExit** | **Boolean** | Can you exit the maze on this tile. |  [optional]
**allowsScoreCollection** | **Boolean** | Does this tile allow for score collection (moving score from your hand to your bag). |  [optional]
**hasBeenVisited** | **Boolean** | Have you visited this tile before. |  [optional]
**rewardOnDestination** | **Integer** | What reward is available on this tile. |  [optional]
**tagOnTile** | **Long** | The tag on this tile. NOTE: default tag is 0 |  [optional]


<a name="DirectionEnum"></a>
## Enum: DirectionEnum
Name | Value
---- | -----
UP | &quot;Up&quot;
RIGHT | &quot;Right&quot;
DOWN | &quot;Down&quot;
LEFT | &quot;Left&quot;



