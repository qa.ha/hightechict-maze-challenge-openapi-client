
# MazeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | The name of the maze. Might give you a hint regarding its structure. |  [optional]
**totalTiles** | **Integer** | How many tiles exist in this maze. |  [optional]
**potentialReward** | **Integer** | The total available reward in this maze. |  [optional]



