
# PlayerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**playerId** | **String** | The public unique identifier of a player. |  [optional]
**name** | **String** | The name a player has chosen to represent her. |  [optional]
**isInMaze** | **Boolean** | An indication of whether a player is currently playing a maze or not. |  [optional]
**maze** | **String** | The name of the maze the player is currently playing. Might be null if player  is not currently playing a maze. |  [optional]
**hasFoundEasterEgg** | **Boolean** | Wink wink. |  [optional]
**mazeScoreInHand** | **Integer** | How much score the player has in her hand. Only available if player is playing a maze. |  [optional]
**mazeScoreInBag** | **Integer** | How much score the player has in her bag. Only available if player is playing a maze. |  [optional]
**playerScore** | **Integer** | The accumulated score across all played mazes. |  [optional]



