# MazeApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**collectScore**](MazeApi.md#collectScore) | **POST** /api/maze/collectScore | 💰 Collect score from your hand to your bag.
[**exitMaze**](MazeApi.md#exitMaze) | **POST** /api/maze/exit | 🚪 Exit the maze.
[**move**](MazeApi.md#move) | **POST** /api/maze/move | Move in the supplied direction.
[**possibleActions**](MazeApi.md#possibleActions) | **GET** /api/maze/possibleActions | 👀 Get the list of possible actions, from the tile where you are standing.
[**tag**](MazeApi.md#tag) | **POST** /api/maze/tag | Tag the current tile with the given (non-negative) number


<a name="collectScore"></a>
# **collectScore**
> PossibleActionsAndCurrentScore collectScore()

💰 Collect score from your hand to your bag.

Remember that when you exit a maze, only score in your bag will carry over and be awarded to your overall player score. Any score left in your hand will be lost.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazeApi apiInstance = new MazeApi();
try {
    PossibleActionsAndCurrentScore result = apiInstance.collectScore();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MazeApi#collectScore");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PossibleActionsAndCurrentScore**](PossibleActionsAndCurrentScore.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exitMaze"></a>
# **exitMaze**
> exitMaze()

🚪 Exit the maze.

Remember that when you exit a maze, only score in your bag will carry over and be awarded to your overall player score. Any score left in your hand will be lost.  Also, remember that you can only play the same maze once, so make sure you have collected as much score as you can.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazeApi apiInstance = new MazeApi();
try {
    apiInstance.exitMaze();
} catch (ApiException e) {
    System.err.println("Exception when calling MazeApi#exitMaze");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="move"></a>
# **move**
> PossibleActionsAndCurrentScore move(direction)

Move in the supplied direction.

You must have already entered a maze. This method will return 200 even if you could not move in this direction. If there is a \&quot;wall\&quot; in your way and you try to move there.. well, it&#39;s gonna hurt, but you will remain in the same place.. which.. technically.. is valid.. 🤷🏻‍

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazeApi apiInstance = new MazeApi();
String direction = "direction_example"; // String | 
try {
    PossibleActionsAndCurrentScore result = apiInstance.move(direction);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MazeApi#move");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **direction** | **String**|  | [enum: Up, Right, Down, Left]

### Return type

[**PossibleActionsAndCurrentScore**](PossibleActionsAndCurrentScore.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="possibleActions"></a>
# **possibleActions**
> PossibleActionsAndCurrentScore possibleActions()

👀 Get the list of possible actions, from the tile where you are standing.

You must have already entered a maze. Also, you shouldn&#39;t require this method that much, given than any action you perform on the maze will return this same information.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazeApi apiInstance = new MazeApi();
try {
    PossibleActionsAndCurrentScore result = apiInstance.possibleActions();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MazeApi#possibleActions");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**PossibleActionsAndCurrentScore**](PossibleActionsAndCurrentScore.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="tag"></a>
# **tag**
> PossibleActionsAndCurrentScore tag(tagValue)

Tag the current tile with the given (non-negative) number

You must have already entered a maze. This method will return 200 even if you already tagged this tile. By tagging a tile \&quot;again\&quot; you will rewrite the previous tag.‍

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazeApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazeApi apiInstance = new MazeApi();
Long tagValue = 789L; // Long | 
try {
    PossibleActionsAndCurrentScore result = apiInstance.tag(tagValue);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MazeApi#tag");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tagValue** | **Long**|  |

### Return type

[**PossibleActionsAndCurrentScore**](PossibleActionsAndCurrentScore.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

