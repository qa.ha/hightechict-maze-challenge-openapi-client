# MazesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**all**](MazesApi.md#all) | **GET** /api/mazes/all | 📜 All the mazes that exist in the game.
[**enter**](MazesApi.md#enter) | **POST** /api/mazes/enter | 🌟 Enter a maze.


<a name="all"></a>
# **all**
> List&lt;MazeInfo&gt; all()

📜 All the mazes that exist in the game.

Even though you can only play a maze once, this method will return all the mazes. This is not laziness from the server side, this is to make it slightly more \&quot;interesting\&quot;, because you need to keep track of the mazes you have already played on your implementation.

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazesApi apiInstance = new MazesApi();
try {
    List<MazeInfo> result = apiInstance.all();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MazesApi#all");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;MazeInfo&gt;**](MazeInfo.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="enter"></a>
# **enter**
> PossibleActionsAndCurrentScore enter(mazeName)

🌟 Enter a maze.

Keep in mind that you can only be playing one maze at a time. Invoking this method when you are already in a maze will result in a failure. Also, you can only play the same maze once. If you wish to play the same maze \&quot;multiple times\&quot;, you need to request to forget your player data (via the player API).

### Example
```java
// Import classes:
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.Configuration;
//import io.swagger.client.auth.*;
//import io.swagger.client.api.MazesApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: User token
ApiKeyAuth User token = (ApiKeyAuth) defaultClient.getAuthentication("User token");
User token.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//User token.setApiKeyPrefix("Token");

MazesApi apiInstance = new MazesApi();
String mazeName = "mazeName_example"; // String | What maze do you wish to enter.
try {
    PossibleActionsAndCurrentScore result = apiInstance.enter(mazeName);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MazesApi#enter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mazeName** | **String**| What maze do you wish to enter. |

### Return type

[**PossibleActionsAndCurrentScore**](PossibleActionsAndCurrentScore.md)

### Authorization

[User token](../README.md#User token)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

